﻿using RightNow.AddIns.AddInViews;
using System.AddIn;
using System.Windows.Forms;

namespace StatusToast
{
    [AddIn("Status: Toast")]
    public class Factory : IStatusBarItem
    {
        private StatusControl statusControl;

        /// <summary>
        /// create our control
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Initialize(IGlobalContext context)
        {
            statusControl = new StatusControl();
            return true;
        }

        /// <summary>
        /// return the control
        /// </summary>
        /// <returns></returns>
        public Control GetControl()
        {
            return statusControl;
        }
    }
}
