﻿/****************************
 * 
 * Written by:  Jack Whitehouse @ 45 North
 * Date:        1/21/14
 * Purpose:     demonstrates workspace control resizing and a known workaround
 * 
 ****************************/

using RightNow.AddIns.AddInViews;
using System.AddIn;

namespace ControlResizing
{
    /// <summary>
    /// standard workspace component launcher code
    /// </summary>
    [AddIn("Control Resizing Demo")]
    public class Launcher : IWorkspaceComponentFactory2
    {
        public IWorkspaceComponent2 CreateControl(bool inDesignMode, IRecordContext context)
        {
            return new WorkspaceControl();
        }

        public System.Drawing.Image Image16
        {
            get { return null; }
        }

        public string Text
        {
            get { return "Automatic Control Resizing"; }
        }

        public string Tooltip
        {
            get { return "Demonstrates the challenges in getting a control to resize properly"; }
        }

        public bool Initialize(IGlobalContext context)
        {
            this.globalContext = context;
            return true;
        }

        public IGlobalContext globalContext { get; set; }
    }
}
