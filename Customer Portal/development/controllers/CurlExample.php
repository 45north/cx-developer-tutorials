<?php
namespace Custom\Controllers;

/**
 * Example of using cURL in a controller
 * @author andy@45n.co
 */
class CurlExample extends \RightNow\Controllers\Base
{
    /**
     * Search using Google Search API
     */
    public function getResults()
    {
        //Load CURL .so file
        \load_curl();

        //Note: GoogleAPI's are available even on Secure Pods.
        //Typically outgoing HTTP connections are blocked on "secure" pods unless the firewall is open to that host
        $url = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=RightNow%20CX";

        //Do cURL stuff
        $ch = \curl_init();
        \curl_setopt($ch, CURLOPT_URL, $url);
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $body = \curl_exec($ch);
        \curl_close($ch);

        // Process the response as JSON
        $json = \json_decode($body);
        
        //Display the Data
        if($json)
        {
            echo "<h1>Results</h1>";
            foreach($json->responseData->results as $searchResult)
            {
                echo $searchResult->title . ": " . $searchResult->url . "<br/>\n\n";
            }
        }

    }
    

}