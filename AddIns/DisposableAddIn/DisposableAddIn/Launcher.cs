﻿using RightNow.AddIns.AddInViews;
using System;
using System.AddIn;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DisposableAddIn
{
    [AddIn("Disposable AddIn")]
    public class Launcher : IWorkspaceComponentFactory2
    {
        public IWorkspaceComponent2 CreateControl(bool inDesignMode, IRecordContext context)
        {
            return new Control(inDesignMode, context);
        }

        public System.Drawing.Image Image16
        {
            get;
            set;
        }

        public string Text
        {
            get { return "Disposable AddIn"; }
        }

        public string Tooltip
        {
            get { return "Disposable AddIn"; }
        }

        public bool Initialize(IGlobalContext context)
        {
            return true;
        }
    }

    public class Control : IWorkspaceComponent2, IDisposable
    {
        private bool inDesignMode;
        private IRecordContext context;
        private WorkspaceControl control;

        public Control(bool inDesignMode, IRecordContext context)
        {
            this.inDesignMode = inDesignMode;
            this.context = context;
            this.control = new WorkspaceControl();

            if (!inDesignMode)
            {
                context.DataLoaded += context_DataLoaded;
            }
        }

        void context_DataLoaded(object sender, EventArgs e)
        {
            control.LoadData();
        }

        #region add-in
        public bool ReadOnly
        {
            get;
            set;
        }

        public void RuleActionInvoked(string actionName)
        {
            throw new NotImplementedException();
        }

        public string RuleConditionInvoked(string conditionName)
        {
            throw new NotImplementedException();
        }

        public System.Windows.Forms.Control GetControl()
        {
            return control;
        }
        #endregion

        /// <summary>
        /// remove the data loaded event and clean up the control
        /// </summary>
        public void Dispose()
        {
            if (control.IsDisposed == false && control.Disposing == false)
            {
                if (inDesignMode == false)
                    context.DataLoaded -= context_DataLoaded;

                control.Dispose();
            }
        }
    }
}
