﻿using System.Windows.Forms;

namespace StatusToast
{
    public partial class StatusControl : UserControl
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public StatusControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// when clicked show the toast
        /// </summary>
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ToastPanel tp = new ToastPanel();

            //offset by the height of the status bar
            tp.Toast(this.ParentForm, yOffset: this.Parent.Parent.Height);
        }
    }
}
