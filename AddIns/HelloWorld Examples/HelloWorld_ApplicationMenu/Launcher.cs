﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RightNow.AddIns.AddInViews;
using System.Drawing;
using System.Windows.Forms;
using System.AddIn;
using HelloWorld_ApplicationMenu.Properties;

namespace HelloWorld_ApplicationMenu
{
    /// <summary>
    /// the first level of app menu buttons. this is shown on the primary menu
    /// </summary>
    [AddIn("App Menu Button")]
    public class AppButton : IAppMenuButton2
    {
        /// <summary>
        /// the button has been clicked
        /// we're updating the context here to show how the name can be changed at run time
        /// </summary>
        public void Click()
        {
            MessageBox.Show("Hello World App Button");
            Context.Text += "-";
        }

        /// <summary>
        /// a 32x32 image to use as an icon for the button
        /// </summary>
        public Image Image
        {
            get { return Resources.ThumbsUp32; }
        }

        /// <summary>
        /// after pressing alt and f this key combo will select the button
        /// </summary>
        public string KeyTips
        {
            get { return "W"; }
        }

        /// <summary>
        /// text to show on the button
        /// </summary>
        public string Text
        {
            get { return "Hello World App Button"; }
        }

        /// <summary>
        /// init the button
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Initialize(IGlobalContext context)
        {
            return true;
        }

        /// <summary>
        /// store the button context, this can be used to update it later
        /// </summary>
        /// <param name="context"></param>
        public void SetMenuItemContext(IMenuItemContext context)
        {
            this.Context = context;
        }
        private IMenuItemContext Context { get; set; }
    }

    /// <summary>
    /// this is a header shown above the button to allow for logical groupings
    /// </summary>
    [AddIn("App Menu Button Header")]
    public class ButtonHeader : IAppMenuButtonHeader2
    {
        /// <summary>
        /// fully qualified name of the root button item
        /// </summary>
        public string ButtonName
        {
            get { return typeof(AppButton).FullName; }
        }
        
        /// <summary>
        /// text to show on the header
        /// </summary>
        public string Text
        {
            get { return "Hello World Header"; }
        }

        /// <summary>
        /// init the header
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Initialize(IGlobalContext context)
        {
            return true;
        }

        /// <summary>
        /// store the context so we can update the header later
        /// </summary>
        /// <param name="context"></param>
        public void SetMenuItemContext(IMenuItemContext context)
        {
            this.Context = context;
        }
        private IMenuItemContext Context { get; set; }
    }

    /// <summary>
    /// this is the button itself
    /// </summary>
    [AddIn("App Menu Button Item")]
    public class Button : IAppMenuButtonItem2
    {
        /// <summary>
        /// if true a new group will be created for this button. groups will be separated by a line
        /// </summary>
        public bool BeginGroup
        {
            get { return true; }
        }

        /// <summary>
        /// fully qualified name of the root button item
        /// </summary>
        public string ButtonName
        {
            get { return typeof(AppButton).FullName; }
        }

        /// <summary>
        /// do something when the button is clicked
        /// </summary>
        public void Click()
        {
            MessageBox.Show("Button: Hello World");
            Context.Text += "-";
        }

        /// <summary>
        /// fully qualified name of the button header
        /// </summary>
        public string HeaderName
        {
            get { return typeof(ButtonHeader).FullName; }
        }

        /// <summary>
        /// after pressing alt and f and W this key combo will select the button
        /// </summary>
        public string KeyTips
        {
            get { return "HW"; }
        }

        /// <summary>
        /// store data on the object
        /// </summary>
        public object Tag
        {
            get;
            set;
        }

        /// <summary>
        /// button text
        /// </summary>
        public string Text
        {
            get { return "Hello World"; }
        }

        /// <summary>
        /// init the button
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Initialize(IGlobalContext context)
        {
            return true;
        }

        /// <summary>
        /// store the button context, this can be used to update it later
        /// </summary>
        /// <param name="context"></param>
        public void SetMenuItemContext(IMenuItemContext context)
        {
            this.Context = context;
        }
        private IMenuItemContext Context { get; set; }
    }
}
