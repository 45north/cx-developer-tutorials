﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RightNow.AddIns.Common;

namespace HelloWorld_ExtensionBar
{
    public partial class ExtensionBarControl : UserControl
    {
        /// <summary>
        /// stores the minimum size of the control, this is used as the default width and height when drawing the control
        /// </summary>
        private Size minSize { get; set; }

        /// <summary>
        /// store the current dock location of the control
        /// </summary>
        public DockingType DockStyle { get; set; }

        /// <summary>
        /// default constructor
        /// </summary>
        public ExtensionBarControl()
        {
            InitializeComponent();

            //store the min dimensions
            minSize = this.MinimumSize;
        }

        /// <summary>
        /// overload the method to return the right size depending on the current dock style
        /// </summary>
        /// <param name="proposedSize"></param>
        /// <returns></returns>
        public override Size GetPreferredSize(Size proposedSize)
        {
            switch (DockStyle)
            {
                case DockingType.Top:
                case DockingType.Bottom:
                    return new Size(proposedSize.Width, minSize.Height);

                case DockingType.Right:
                case DockingType.Left:
                    return new Size(minSize.Width, proposedSize.Height);

                case DockingType.Floating:
                    return new Size(200, 200); //any size that's appropriate for you control
            }

            return base.GetPreferredSize(proposedSize);
        }
    }
}
