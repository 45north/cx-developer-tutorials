﻿using SOAP_Account_PasswordReset.RNT.SOAP;
using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Windows.Forms;

namespace SOAP_Account_PasswordReset
{
    public partial class Form1 : Form
    {
        private readonly string SITE = "https://**SITE**.custhelp.com/cgi-bin/**INTERFACE**.cfg/services/soap";
        private readonly string USER = "";
        private readonly string PWD = "";
        private readonly int ACCOUNT_ID = 0;

        private RightNowSyncPortClient client;
        private ClientInfoHeader clientHeader;

        public Form1()
        {
            InitializeComponent();

            EndpointAddress endPointAddr = new EndpointAddress(SITE);

            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential);
            binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName;
            binding.ReceiveTimeout = new TimeSpan(0, 10, 0);
            binding.MaxReceivedMessageSize = 10485760; //10MB
            binding.SendTimeout = new TimeSpan(0, 10, 0);

            client = new RightNowSyncPortClient(binding, endPointAddr);
            client.ClientCredentials.UserName.UserName = USER;
            client.ClientCredentials.UserName.Password = PWD;

            BindingElementCollection elements = client.Endpoint.Binding.CreateBindingElements();
            elements.Find<SecurityBindingElement>().IncludeTimestamp = false;

            client.Endpoint.Binding = new CustomBinding(elements);

            clientHeader = new ClientInfoHeader
            {
                AppID = "SOAP_Account_PasswordReset"
            };
        }

        #region button handlers
        private void button_pwd_Click(object sender, EventArgs e)
        {
            SetPassword();
        }

        private void button_reset_Click(object sender, EventArgs e)
        {
            SetReset();
        }

        private void button_all_Click(object sender, EventArgs e)
        {
            SetPasswordAndReset();
        }
        #endregion

        private void SetPassword()
        {
            Account acct = new Account
            {
                ID = new ID
                {
                    id = ACCOUNT_ID,
                    idSpecified = true,
                },
                NewPassword = GenerateRandomPassword()
            };

            client.Update(clientHeader, 
                new RNObject[] 
                {
                    acct
                }, 
                new UpdateProcessingOptions 
                { 
                    SuppressExternalEvents = true, 
                    SuppressRules = true 
                });
        }

        private void SetReset()
        {
            Account acct = new Account
            {
                ID = new ID
                {
                    id = ACCOUNT_ID,
                    idSpecified = true,
                },
                Attributes = new AccountOptions
                {
                    ForcePasswordChange = true,
                    ForcePasswordChangeSpecified = true
                }
            };

            client.Update(clientHeader, 
                new RNObject[] 
                { 
                    acct
                }, 
                new UpdateProcessingOptions 
                {
                    SuppressExternalEvents = true, 
                    SuppressRules = true 
                });
        }

        private void SetPasswordAndReset()
        {
            Account acct = new Account
            {
                ID = new ID
                {
                    id = ACCOUNT_ID,
                    idSpecified = true,
                },
                NewPassword = GenerateRandomPassword(),
                Attributes = new AccountOptions
                {
                    ForcePasswordChange = true,
                    ForcePasswordChangeSpecified = true
                }
            };

            client.Update(clientHeader, 
                new RNObject[] 
                { 
                    acct 
                },
                new UpdateProcessingOptions
                { 
                    SuppressExternalEvents = true, 
                    SuppressRules = true 
                });
        }

        private static string GenerateRandomPassword()
        {
            Random rand = new Random();

            string pwdChars = "";

            pwdChars += "abcdefghijklmnopqrstuvwyxz";
            pwdChars += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            pwdChars += "1234567890";
            pwdChars += "<>,./?;:'\"[]{}\\|~!@#$%^&*()_+-=";

            string output = "";

            for (int i = 0; i < 16; i++)
            {
                output += pwdChars[rand.Next(0, pwdChars.Length)];
            }

            return output;
        }
    }
}
