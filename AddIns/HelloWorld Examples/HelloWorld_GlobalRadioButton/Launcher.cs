﻿using HelloWorld_GlobalRadioButton.Properties;
using RightNow.AddIns.AddInViews;
using System;
using System.AddIn;
using System.Drawing;
using System.Windows.Forms;

namespace HelloWorld_GlobalRadioButton
{
    /// <summary>
    /// ribbon tabs are the highest level of control. it contains a ribbon group
    /// </summary>
    [AddIn("Global Ribbon Tab Example")]
    public class Tab : IGlobalRibbonTab
    {
        /// <summary>
        /// alt + this key to switch to the tab
        /// </summary>
        public string KeyTips
        {
            get { return "C"; }
        }

        /// <summary>
        /// display order of the tab
        /// </summary>
        public int Order
        {
            get { return 0; }
        }

        /// <summary>
        /// text to show on the button group
        /// </summary>
        public string Text
        {
            get { return "CX Developer"; }
        }

        /// <summary>
        /// control the visibility of the group
        /// </summary>
        public bool Visible
        {
            get { return true; }
        }

        /// <summary>
        /// init the group
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Initialize(IGlobalContext context)
        {
            return true;
        }

        public event EventHandler VisibleChanged;
    }

    /// <summary>
    /// a ribbon group is contained by a ribbon tab. the group contains any number of ribbon buttons
    /// </summary>
    [AddIn("Global Ribbon Group Example")]
    public class Group : IGlobalRibbonGroup
    {
        /// <summary>
        /// display order of the group
        /// </summary>
        public int Order
        {
            get { return 0; }
        }

        /// <summary>
        /// fully qualified name of the tab that hosts it
        /// </summary>
        public string TabName
        {
            get { return typeof(Tab).FullName; }
        }

        /// <summary>
        /// this is the name of the group
        /// </summary>
        public string Text
        {
            get { return "Examples"; }
        }

        /// <summary>
        /// controls visibility to the group
        /// </summary>
        public bool Visible
        {
            get { return true; }
        }

        /// <summary>
        /// init the control
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Initialize(IGlobalContext context)
        {
            return true;
        }

        public event EventHandler VisibleChanged;
    }

    /// <summary>
    /// a ribbon button is contained within a ribbon group
    /// </summary>
    [AddIn("Global Ribbon Button Example")]
    public class HW_Button : IGlobalRibbonButton
    {
        /// <summary>
        /// what to do when the button is clicked
        /// </summary>
        public void Click()
        {
            MessageBox.Show("Hello World");
        }

        /// <summary>
        /// return true to enable the button. 
        /// if this value changes then EnabledChanged needs to be raised
        /// </summary>
        public bool Enabled
        {
            get { return true; }
        }

        /// <summary>
        /// fully qualified name of the button group
        /// </summary>
        public string GroupName
        {
            get { return typeof(Group).FullName; }
        }

        /// <summary>
        /// 16x16 image to use for the button icon
        /// </summary>
        public Image Image16
        {
            get { return Resources.ThumbsUp16; }
        }

        /// <summary>
        /// 32x32 image to use for the button icon
        /// </summary>
        public Image Image32
        {
            get { return Resources.ThumbsUp32; }
        }

        /// <summary>
        /// alt + this key to "click" the button
        /// </summary>
        public string KeyTips
        {
            get { return "H"; }
        }

        /// <summary>
        /// display order of the button within the group
        /// </summary>
        public int Order
        {
            get { return 0; }
        }

        /// <summary>
        /// shortcut key to "click" the button
        /// (generally this should not be used as it will capture that button regardless of context)
        /// </summary>
        public Keys Shortcut
        {
            get;
            set;
        }

        /// <summary>
        /// name of the button
        /// </summary>
        public string Text
        {
            get { return "Hello World"; }
        }

        /// <summary>
        /// subtext for the button
        /// </summary>
        public string Tooltip
        {
            get { return ""; }
        }

        /// <summary>
        /// control the visibility of the button
        /// </summary>
        public bool Visible
        {
            get { return true; }
        }

        /// <summary>
        /// init the control
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Initialize(IGlobalContext context)
        {
            return true;
        }

        public event EventHandler VisibleChanged;
        public event EventHandler EnabledChanged;
    }

    /// <summary>
    /// A slightly more complicated example where the button hides itself once the button is clicked
    /// </summary>
    [AddIn("Global Ribbon Button Example 2")]
    public class GB_Button : IGlobalRibbonButton
    {
        /// <summary>
        /// what to do when the button is clicked
        /// </summary>
        public void Click()
        {
            MessageBox.Show("Goodbye World");
            Visible = !Visible;
        }

        /// <summary>
        /// return true to enable the button. 
        /// if this value changes then EnabledChanged needs to be raised
        /// </summary>
        public bool Enabled
        {
            get { return true; }
        }

        /// <summary>
        /// fully qualified name of the button group
        /// </summary>
        public string GroupName
        {
            get { return typeof(Group).FullName; }
        }

        /// <summary>
        /// small image to show
        /// </summary>
        public Image Image16
        {
            get { return Resources.ThumbsUp16; }
        }

        /// <summary>
        /// large image to show
        /// </summary>
        public Image Image32
        {
            get { return Resources.ThumbsUp32; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string KeyTips
        {
            get { return "G"; }
        }

        /// <summary>
        /// display order of the buttons in the group
        /// </summary>
        public int Order
        {
            get { return 1; }
        }

        /// <summary>
        /// pressing this key will "click" the button
        /// (generally this should not be used as it will capture that button regardless of context)
        /// </summary>
        public Keys Shortcut
        {
            get;
            set;
        }

        /// <summary>
        /// text to show on the button
        /// </summary>
        public string Text
        {
            get { return "Goodbye World"; }
        }

        /// <summary>
        /// mouseover text for the button
        /// </summary>
        public string Tooltip
        {
            get { return "Goodbye world tooltip"; }
        }

        /// <summary>
        /// control the visibility of the button
        /// </summary>
        public bool Visible
        {
            get { return _visible; }
            private set
            {
                _visible = value;

                //raise the event if something is listening for it
                if (VisibleChanged != null)
                {
                    VisibleChanged(this, new EventArgs());
                }
            }
        }
        private bool _visible = true;

        /// <summary>
        /// init the button
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Initialize(IGlobalContext context)
        {
            return true;
        }

        public event EventHandler VisibleChanged;
        public event EventHandler EnabledChanged;
    }
}
