﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.AddIn;
using System.Drawing;
using System.Windows.Forms;
using RightNow.AddIns.AddInViews;
using HelloWorld_WorkspaceComponent.Properties;


namespace HelloWorld_WorkspaceComponent
{
    /// <summary>
    /// creates a ui component that can be added to a workspace
    /// </summary>
    [AddIn("Hello World", Version="1.0.0.0")]
    public class Factory : IWorkspaceComponentFactory2
    {
        /// <summary>
        /// return the component created below
        /// </summary>
        /// <param name="inDesignMode"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public IWorkspaceComponent2 CreateControl(bool inDesignMode, IRecordContext context)
        {
            return new Component(inDesignMode, context);
        }

        /// <summary>
        /// a 16x16 icon to use in the workspace designer
        /// </summary>
        public Image Image16
        {
            get { return Resources.ThumbsUp16; }
        }

        /// <summary>
        /// text to show in the workspace designer
        /// </summary>
        public string Text
        {
            get { return "Hello World"; }
        }

        /// <summary>
        /// tooltip to show in the workspace designer
        /// </summary>
        public string Tooltip
        {
            get { return "A Simple Example"; }
        }

        /// <summary>
        /// init the control
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Initialize(IGlobalContext context)
        {
            return true;
        }
    }

    public class Component : IWorkspaceComponent2
    {
        /// <summary>
        /// set to true if the control is in a workspace designer
        /// </summary>
        private bool inDesignMode;

        /// <summary>
        /// the workspace control
        /// </summary>
        private WorkspaceControl control;
        private IRecordContext context;

        /// <summary>
        /// create the component
        /// </summary>
        /// <param name="inDesignMode">store the inDesignMode flag</param>
        /// <param name="context">information about the workspace</param>
        public Component(bool inDesignMode, IRecordContext context)
        {
            // TODO: Complete member initialization
            this.inDesignMode = inDesignMode;
            this.context = context;

            //create the UI control and store it
            control = new WorkspaceControl(inDesignMode);
        }

        /// <summary>
        /// return true to set the control to read only
        /// </summary>
        public bool ReadOnly
        {
            get;
            set;
        }

        public void RuleActionInvoked(string actionName)
        {
            throw new NotImplementedException();
        }

        public string RuleConditionInvoked(string conditionName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// return the reference to the control
        /// </summary>
        /// <returns></returns>
        public Control GetControl()
        {
            return control;
        }
    }

}
