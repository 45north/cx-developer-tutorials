﻿using HelloWorld_NavigationSection.Properties;
using RightNow.AddIns.AddInViews;
using System;
using System.AddIn;
using System.Drawing;
using System.Windows.Forms;

namespace HelloWorld_NavigationSection
{
    /// <summary>
    /// creates a new navigation section on the left the screen and places a custom UI component in it
    /// </summary>
    [AddIn("HelloWorld NavigationSection")]
    public class Factory : INavigationSectionFactory2
    {
        /// <summary>
        /// return the nav section defined below
        /// </summary>
        /// <param name="inDesignMode"></param>
        /// <returns></returns>
        public INavigationSection2 CreateControl(bool inDesignMode)
        {
            return new Component(inDesignMode);
        }

        /// <summary>
        /// should be: icon to show in nav section designer
        /// however it appears to be unused
        /// </summary>
        public Image Image16
        {
            get { return Resources.ThumbsUp16; }
        }

        /// <summary>
        /// should be: text to show in nav section designer
        /// however it appears to be unused
        /// </summary>
        public string Text
        {
            get { return "Hello World Nav Section"; }
        }

        /// <summary>
        /// tooltip to show in nav section designer
        /// </summary>
        public string Tooltip
        {
            get { return "NavSection Tooltip"; }
        }

        /// <summary>
        /// init the factory
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Initialize(IGlobalContext context)
        {
            return true;
        }
    }

    /// <summary>
    /// nav section component
    /// </summary>
    public class Component : INavigationSection2
    {
        private bool inDesignMode;
        private NavSectionControl control { get; set; }

        /// <summary>
        /// not a part of the interface, but a useful addition
        /// </summary>
        /// <param name="inDesignMode"></param>
        public Component(bool inDesignMode)
        {
            this.inDesignMode = inDesignMode;

            control = new NavSectionControl();
        }

        /// <summary>
        /// text to show on the nav section header
        /// </summary>
        public string HeaderText
        {
            get { return "Hello World Nav Section"; }
        }

        /// <summary>
        /// return the UI component
        /// </summary>
        /// <returns></returns>
        public Control GetControl()
        {
            return control;
        }
    }
}
