﻿using StatusToast;
using System;
using System.Windows.Forms;

namespace TestForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void toolStripSplitButton1_ButtonClick(object sender, EventArgs e)
        {
            ToastPanel tp = new ToastPanel();
            tp.Toast(this);
        }
    }
}
