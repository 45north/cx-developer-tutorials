﻿using RightNow.AddIns.AddInViews;
using System.AddIn;
using System.Windows.Forms;

namespace HelloWorld_StatusBar
{
    /// <summary>
    /// creates a control that lives in the status bar of the cx console
    /// </summary>
    [AddIn("HelloWorld_StatusBar")]
    public class Launcher : IStatusBarItem
    {
        private StatusBarControl control;

        /// <summary>
        /// create the control
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Initialize(IGlobalContext context)
        {
            control = new StatusBarControl();
            return true;
        }

        /// <summary>
        /// return the control
        /// </summary>
        /// <returns></returns>
        public Control GetControl()
        {
            return control;
        }
    }
}