﻿using HelloWorld_ContentPane.Properties;
using RightNow.AddIns.AddInViews;
using System.AddIn;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace HelloWorld_ContentPane
{
    /// <summary>
    /// content pane is used to create a top level tab with any information. it is not tied to a record context
    /// </summary>
    public class Component : IContentPaneControl
    {
        /// <summary>
        /// occurs before the tab closes
        /// </summary>
        /// <returns>return false to stop the tab from closing</returns>
        public bool BeforeClose()
        {
            if (MessageBox.Show("Allow Close?", "Closing", MessageBoxButtons.YesNo) == DialogResult.Yes)
                return true;
            else
                return false;
        }

        /// <summary>
        /// occurs when the tab has been closed
        /// </summary>
        public void Closed()
        {
            MessageBox.Show("Closed");
        }

        /// <summary>
        /// 16x16 image to display on the tab
        /// </summary>
        public Image Image16
        {
            get { return Resources.ThumbsUp16; }
        }

        /// <summary>
        /// used to associate ribbon buttons with the content pane control
        /// </summary>
        public IList<IEditorRibbonButton> RibbonButtons
        {
            get { return new List<IEditorRibbonButton>(); }
        }

        /// <summary>
        /// text to show on the tab
        /// </summary>
        public string Text
        {
            get { return "Hello World Content Pane"; }
        }

        /// <summary>
        /// the unique ID is used to find the content pane. here it's set up so that
        /// only a single content pane of this type will be created, if multiple are desired
        /// then generate a guid for each instance
        /// </summary>
        public string UniqueID
        {
            get { return Component.UID; }
        }

        /// <summary>
        /// store the guid here so that we can easily get back to it from other classes
        /// </summary>
        public static string UID = "{BFD34067-FB8B-4927-AB7E-38B142B0B4AA}";

        /// <summary>
        /// return an instance of the control to render in the content pane
        /// </summary>
        /// <returns></returns>
        public Control GetControl()
        {
            return new ContentControl();
        }
    }

    /// <summary>
    /// this is needed to launch the content pane. other types of add-ins would work as well
    /// </summary>
    [AddIn("Content Pane Launcher", Version = "1.0")]
    public class Launcher : IAppMenuButton2
    {
        /// <summary>
        /// create a new content pane from the class above
        /// </summary>
        public void Click()
        {
            //first try to find it using the unique ID, then open it if that fails
            if (context.AutomationContext.FindAndFocus(Component.UID) == false)
                context.AutomationContext.OpenEditor(new Component());
        }

        #region not related to the content pane
        public Image Image
        {
            get;
            set;
        }

        public string KeyTips
        {
            get;
            set;
        }

        public void SetMenuItemContext(IMenuItemContext context)
        {
        }

        public string Text
        {
            get { return "Content Pane Test"; }
        }

        public bool Initialize(IGlobalContext context)
        {
            this.context = context;
            return true;
        }

        public IGlobalContext context { get; set; }
        #endregion
    }


}
