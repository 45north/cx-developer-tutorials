﻿/****************************
 * 
 * Written by:  Jack Whitehouse @ 45 North
 * Date:        08/11/13
 * Purpose:     Display how easy it is to break workspace configs
 * 
 ****************************/
using RightNow.AddIns.AddInViews;
using System.Windows.Forms;

namespace WorkspaceConfig
{
    public partial class WorkspaceControl : UserControl
    {
        public WorkspaceControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// setting this property will update the working label
        /// </summary>
        public string ThisWorks
        {
            get
            {
                return label_works.Text;
            }
            set
            {
                label_works.Text = value;
            }
        }

        /// <summary>
        /// setting this property will update the broken label
        /// </summary>
        public string DoesNotWork
        {
            get
            {

                return label_doesntWork.Text;
            }
            set
            {
                label_doesntWork.Text = value;
            }
        }

    }
}
