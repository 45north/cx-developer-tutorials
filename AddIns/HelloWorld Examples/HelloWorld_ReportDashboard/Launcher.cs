﻿using HelloWorld_ReportDashboard.Properties;
using RightNow.AddIns.AddInViews;
using System.AddIn;
using System.Drawing;
using System.Windows.Forms;

namespace HelloWorld_ReportDashboard
{
    /// <summary>
    /// factory for a report dashboard component
    /// </summary>
    [AddIn("HelloWorld ReportDashboard")]
    public class Factory : IDashboardComponentFactory
    {
        /// <summary>
        /// return the component defined below
        /// </summary>
        /// <param name="inDesignMode"></param>
        /// <returns></returns>
        public IDashboardComponent CreateControl(bool inDesignMode)
        {
            return new Component(inDesignMode);
        }

        /// <summary>
        /// 24x24 image
        /// </summary>
        public Image Image24
        {
            get { return Resources.ThumbsUp24; }
        }

        /// <summary>
        /// 16x16 image
        /// </summary>
        public Image Image16
        {
            get { return Resources.ThumbsUp16; }
        }

        /// <summary>
        /// header text for the dashboard component
        /// </summary>
        public string Text
        {
            get { return "Hello World Report Dashboard"; }
        }

        /// <summary>
        /// tooltip for the component
        /// </summary>
        public string Tooltip
        {
            get { return "Hello World Report Dashboard Tooltip"; }
        }

        /// <summary>
        /// init the control
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Initialize(IGlobalContext context)
        {
            return true;
        }
    }

    /// <summary>
    /// create the dashboard component
    /// </summary>
    public class Component : IDashboardComponent
    {
        private bool inDesignMode;
        private ReportControl control;

        /// <summary>
        /// not a part of the interface, but a useful tool
        /// </summary>
        /// <param name="inDesignMode"></param>
        public Component(bool inDesignMode)
        {
            this.inDesignMode = inDesignMode;
            control = new ReportControl();
        }

        /// <summary>
        /// return the control
        /// </summary>
        /// <returns></returns>
        public Control GetControl()
        {
            return control;
        }
    }

}