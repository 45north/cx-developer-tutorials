﻿using CWS_AddIn.Properties;
using RightNow.AddIns.AddInViews;
using System;
using System.AddIn;
using System.Drawing;
using System.Windows.Forms;

namespace CWS_AddIn
{
    [AddIn("CWS AddIn")]
    public class Factory : IWorkspaceComponentFactory2
    {
        /// <summary>
        /// pass the global context up to the control
        /// </summary>
        private IGlobalContext globalContext { get; set; }

        /// <summary>
        /// create the component and pass all of the information along to it
        /// </summary>
        /// <param name="inDesignMode"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public IWorkspaceComponent2 CreateControl(bool inDesignMode, IRecordContext context)
        {
            return new Component(inDesignMode, context, globalContext);
        }

        /// <summary>
        /// an icon for the workspace designer
        /// </summary>
        public Image Image16
        {
            get { return Resources.ThumbsUp16; }
        }

        /// <summary>
        /// name of the add-in
        /// </summary>
        public string Text
        {
            get { return "AddIn with CWS"; }
        }

        /// <summary>
        /// tooltip for the add-in
        /// </summary>
        public string Tooltip
        {
            get { return "This addin uses Connect Web Services to retrieve data"; }
        }

        /// <summary>
        /// init the add-in and connect to the SOAP API
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Initialize(IGlobalContext context)
        {
            globalContext = context;

            //it would be nice to be able to check context.CanAccessPublicSoap 
            //but the property isn't related to the session auth checkbox

            try
            {
                //set up soap based on the global context for the site
                RightNowSoapClient.Instance.Init(context);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to connect to CWS: " + ex.Message);
                context.LogMessage("Failed to connect to CWS: " + ex);
                return false;
            }
        }
    }

    /// <summary>
    /// add-in component
    /// </summary>
    public class Component : IWorkspaceComponent2
    {
        /// <summary>
        /// store the control
        /// </summary>
        private WorkspaceControl control;

        /// <summary>
        /// overloaded constructor
        /// </summary>
        /// <param name="inDesignMode">true if we're on a workspace designer</param>
        /// <param name="recordContext">information about the workspace</param>
        /// <param name="globalContext">information about the session</param>
        public Component(bool inDesignMode, IRecordContext recordContext, IGlobalContext globalContext)
        {
            //create the control and pass all of the information up to it
            control = new WorkspaceControl(inDesignMode, recordContext, globalContext);

            //if we're not on a workspace designer listen for the data to finish loading and
            //then load the control information
            if (!inDesignMode)
            {
                //listen for the workspace to finish loading
                recordContext.DataLoaded += (o, e) =>
                {
                    control.LoadData();
                };
            }
        }

        /// <summary>
        /// return the control
        /// </summary>
        /// <returns></returns>
        public Control GetControl()
        {
            return control;
        }

        #region unused properties
        public bool ReadOnly { get; set; }

        public void RuleActionInvoked(string actionName){}

        public string RuleConditionInvoked(string conditionName){return ""; }
        #endregion
    }

}
