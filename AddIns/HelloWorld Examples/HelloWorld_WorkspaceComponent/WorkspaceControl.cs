﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HelloWorld_WorkspaceComponent
{
    public partial class WorkspaceControl : UserControl
    {
        private bool inDesignMode;

        public WorkspaceControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// AddIn constructor: stores the design mode flag 
        /// and lets the standard constructor create the UI
        /// </summary>
        /// <param name="inDesignMode">true if we're on a workspace designer</param>
        public WorkspaceControl(bool inDesignMode) : this()
        {
            this.inDesignMode = inDesignMode;
        }
    }
}
