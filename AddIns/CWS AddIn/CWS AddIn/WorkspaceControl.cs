﻿using CWS_AddIn.RNT_SOAP;
using RightNow.AddIns.AddInViews;
using System.Windows.Forms;

namespace CWS_AddIn
{
    /// <summary>
    /// User control for CWS AddIn
    /// </summary>
    public partial class WorkspaceControl : UserControl
    {
        #region private vars
        /// <summary>
        /// true if we're on a workspace designer
        /// </summary>
        private bool inDesignMode;

        /// <summary>
        /// information about the workspace
        /// </summary>
        private IRecordContext recordContext;

        /// <summary>
        /// information about the session
        /// </summary>
        private IGlobalContext globalContext;

        /// <summary>
        /// account record for the current user, populated by CWS
        /// </summary>
        private Account AccountRecord;
        #endregion

        /// <summary>
        /// keep the default constructor so that Visual Studio can render the control
        /// </summary>
        public WorkspaceControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// store the information from the add-in framework and set up our listeners
        /// </summary>
        /// <param name="inDesignMode">true if we're on a workspace designer</param>
        /// <param name="recordContext">information about the workspace</param>
        /// <param name="globalContext">information about the session</param>
        /// <remarks>calls default constructor to init UI</remarks>
        public WorkspaceControl(bool inDesignMode, IRecordContext recordContext, IGlobalContext globalContext) : this()
        {
            this.inDesignMode = inDesignMode;
            this.recordContext = recordContext;
            this.globalContext = globalContext;

            //if we're on a workspace then listen for the asyn call to complete
            if (!inDesignMode)
            {
                RightNowSoapClient.Instance.client.GetCompleted += client_GetCompleted;
            }
        }

        /// <summary>
        /// the workspace has finished loading. load the account from CWS
        /// </summary>
        internal void LoadData()
        {
            //make sure we connected successfully
            if (RightNowSoapClient.Instance.IsConnected)
            {
                //get the account from CWS
                RightNowSoapClient.Instance.client.GetAsync(
                    RightNowSoapClient.Instance.headerInfo,     //use the soap header created by the singleton
                    new RNObject[]                              //create an array of RN objects and populate it with an account template
                    {
                        new Account                             //the type of object we want to retrieve
                        {
                            ID = new ID                         
                            {
                                id = globalContext.AccountId,   //the ID of the account to pull down
                                idSpecified = true              //necessary because id is nullable
                            }
                        }
                    },
                    new GetProcessingOptions { FetchAllNames = true }); //make sure to populate labels for any NamedIDs
            }
        }

        /// <summary>
        /// aync call has completed, bind our data to the UI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void client_GetCompleted(object sender, GetCompletedEventArgs e)
        {
            //make sure that the correct type of data was returned
            if (e.Result != null && 
                e.Result.Length > 0 && 
                e.Result[0] is Account)
            {
                //store the record
                AccountRecord = e.Result[0] as Account;

                //bind the results to the UI
                accountBindingSource.DataSource = AccountRecord;
            }
        }
    }
}
