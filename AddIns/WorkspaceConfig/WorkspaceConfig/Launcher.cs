﻿/****************************
 * 
 * Written by:  Jack Whitehouse @ 45 North
 * Date:        08/11/13
 * Purpose:     Display how easy it is to break workspace configs
 * 
 ****************************/
using RightNow.AddIns.AddInViews;
using System.AddIn;
using System.Drawing;
using WorkspaceConfig.Properties;

namespace WorkspaceConfig
{
    [AddIn("Workspace Config Test Control and Something", Version = "1.0.0.1")]
    public class Factory : IWorkspaceComponentFactory2
    {
        #region add-in factory
        public IWorkspaceComponent2 CreateControl(bool inDesignMode, IRecordContext context)
        {
            return new Component(inDesignMode, context);
        }

        public Image Image16
        {
            get { return Resources.ThumbsUp16; }
        }

        public string Text
        {
            get { return "Workspace Config"; }
        }

        public string Tooltip
        {
            get { return "Used to test workspace configs"; }
        }

        public bool Initialize(IGlobalContext context)
        {
            return true;
        }
        #endregion
    }

    public class Component : IWorkspaceComponent2
    {
        /// <summary>
        /// this workspace config will properly update the label
        /// </summary>
        [WorkspaceConfigProperty("ThisWorks",
            Description = "Settings stored here will be kept",
            NameInDesigner = "This Works")]
        public string ThisWorks
        {
            get
            {
                return control.ThisWorks;
            }
            set
            {
                control.ThisWorks = value;
            }
        }

        /// <summary>
        /// this workspace config will not properly update the label
        /// </summary>
        [WorkspaceConfigProperty("ThisDoesNotWork",
            Description = "Settings stored here will not be kept",
            NameInDesigner = "This Does Not Work")]
        public string ThisDoesNotWork
        {
            get
            {
                return control.DoesNotWork;
            }
            set
            {
                control.DoesNotWork = value;
            }
        }

        #region add-in component
        public Component(bool inDesignMode, IRecordContext context)
        {
            control = new WorkspaceControl();
        }

        public bool ReadOnly
        {
            get;
            set;
        }

        public void RuleActionInvoked(string actionName)
        {
        }

        public string RuleConditionInvoked(string conditionName)
        {
            return "";
        }

        public System.Windows.Forms.Control GetControl()
        {
            return control;
        }
        private WorkspaceControl control;
        #endregion
    }

}
