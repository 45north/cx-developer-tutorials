﻿using HelloWorld_WorkspaceRibbon.Properties;
using RightNow.AddIns.AddInViews;
using System.AddIn;
using System.Drawing;
using System.Windows.Forms;

namespace HelloWorld_WorkspaceRibbon
{
    /// <summary>
    /// adds a ribbon button to a workspace
    /// </summary>
    [AddIn("HelloWorld WorkspaceRibbon")]
    public class Factory : IWorkspaceRibbonButtonFactory
    {
        /// <summary>
        /// create the workspace ribbon button
        /// </summary>
        /// <param name="inDesignMode"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public IWorkspaceRibbonButton CreateControl(bool inDesignMode, IRecordContext context)
        {
            return new Button(inDesignMode, context);
        }

        /// <summary>
        /// 32x32 icon
        /// </summary>
        public Image Image32
        {
            get { return Resources.ThumbsUp32; }
        }

        /// <summary>
        /// 16x16 icon
        /// </summary>
        public Image Image16
        {
            get { return Resources.ThumbsUp16; }
        }

        /// <summary>
        /// text to show on the button
        /// </summary>
        public string Text
        {
            get { return "Hello World Workspace Ribbon"; }
        }

        /// <summary>
        /// tooltip for the button
        /// </summary>
        public string Tooltip
        {
            get { return "Hello World Workspace Ribbon Tooltip"; }
        }

        /// <summary>
        /// init the ribbon control
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Initialize(IGlobalContext context)
        {
            return true;
        }
    }

    /// <summary>
    /// workspace button
    /// </summary>
    public class Button : IWorkspaceRibbonButton
    {
        private bool inDesignMode;
        private IRecordContext context;

        /// <summary>
        /// not part of the interface, but useful information to pass along
        /// </summary>
        /// <param name="inDesignMode"></param>
        /// <param name="context"></param>
        public Button(bool inDesignMode, IRecordContext context)
        {
            this.inDesignMode = inDesignMode;
            this.context = context;
        }

        /// <summary>
        /// what to do when the button is clicked
        /// </summary>
        public void Click()
        {
            MessageBox.Show("Hello World");
        }
    }

}
