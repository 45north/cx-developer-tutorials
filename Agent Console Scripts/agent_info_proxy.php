<?php
    
    /**
     *  AgentInfoProxy
     *  For use with Browser Controls in CX Agent Console. Script takes the Agent's
     *  Session ID supplied by a GET parameter, authenticates the Agent, then retrieves
     *  additional agent data. Finally a redirect is performed to an external URL with
     *  the additional Agent data.
     *
     *  Usage:
     *  Upload the script through the File Manager workspace in the CX Agent Console
     *  to the 'custom scripts' directory.
     *  Through the Workspace Editor, open a workspace and place a Browser Control
     *  Configure the Browser Control to point to the following URL:
     *  https://your_site.custhelp.com/cgi-bin/your_interface.cfg/php/custom/agent_info_proxy.php?p_sid=$p_sid
     *  
     *  @TODO - make redirect URL a GET parameter
     *
     *  @author: andy@45n.co
     *  @url: 
     *  @version: 1.0  
     *
     */
    try 
    {
        //Redirect URL with placeholder for Agent Login
        //>>>>>> Change this value for your own use <<<<<<
        $url = "https://www.google.com#q=%s";
    

        if (!isset($_GET['p_sid']) || empty($_GET['p_sid'])) 
        {
            throw new \Exception("Missing required GET parameter: p_sid");
        }
        
        $sessionId = $_GET['p_sid'];
        
        // Set up and call the AgentAuthenticator
        require_once (get_cfg_var('doc_root') . '/include/services/AgentAuthenticator.phph');
        
        $account = AgentAuthenticator::authenticateSessionID($sessionId);
        if (!isset($account)) 
        {
            throw new \Exception("Couldn't Authenticate Agent using Session ID");
        }
        
        //Use Agent ID to retrieve Staff Account object using ConnectPHP
        $agentObj = \RightNow\Connect\v1_2\Account::fetch((int) $account['acct_id']);
        
        //Construct Redirect URL with Agent values
        $url = sprintf($url, $agentObj->Login);
        
        //Perform Redirect
        header("Location: $url");
        exit();
        
        
    }
    catch(\Exception $ex)
    {
        echo "No input file specified.";
        exit;
    }