﻿using HelloWorld_NavigationItem.Properties;
using RightNow.AddIns.AddInViews;
using System.AddIn;
using System.Drawing;
using System.Windows.Forms;

namespace HelloWorld_NavigationItem
{
    /// <summary>
    /// shows a link in an existing nav section
    /// </summary>
    [AddIn("HelloWorld NavigationItem")]
    public class Component : INavigationItem
    {
        /// <summary>
        /// what to do when the button is clicked
        /// </summary>
        public void Activate()
        {
            MessageBox.Show("Hello World");
        }

        /// <summary>
        /// this can't be null. it will cause the designer to throw an exception
        /// </summary>
        public Image Image16
        {
            get { return Resources.ThumbsUp16; }
        }

        /// <summary>
        /// text to show on the button
        /// </summary>
        public string Text
        {
            get { return "Hello World"; }
        }

        /// <summary>
        /// init the control
        /// </summary>
        /// <param name="context">session context</param>
        /// <returns></returns>
        public bool Initialize(IGlobalContext context)
        {
            return true;
        }
    }
}
